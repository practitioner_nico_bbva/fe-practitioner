//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 1123;

app.use(express.static(__dirname));

app.get('/', (req, res)=> {
  res.status(200).sendFile("index.html", { root: "." });
})

app.listen(port);
console.log("http://localhost:"+ port );
