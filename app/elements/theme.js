import { setComponentSharedStyles, setDocumentCustomStyles } from '@cells-components/cells-lit-helpers';
import { css } from 'lit-element';

setComponentSharedStyles('cells-template-paper-drawer-panel-shared-styles', css`
  :host { display: block !important; }
`);

setComponentSharedStyles('abm-schedules-shared-styles', css`
  :host { padding: 0 15px; margin-top: 10%; }
`);

setDocumentCustomStyles(css`
  *{
    font-family: 'Roboto'; 
  }
`)