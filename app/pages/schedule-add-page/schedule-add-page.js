import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';
import '@bbva-web-components/bbva-spinner';
import '@bbva-web-components/bbva-header-main';
import '@practitioner/dm-schedule';
import '@practitioner/abm-schedules';


class ScheduleAddPage extends CellsPage {

  static get properties() {
    return {
      loading: Boolean,
      schedules: Array,
      params: {
        type: Object
      }
    };
  }

  static get styles() {
    return css`
      /* bbva-spinner { height: 100vh; position: fixed; width: 100%; z-index: 1000; background: white; top: 0; } */
    `;
  }

  constructor() {
    super();
    this.loading = true;
  }

  _hideSpinner({ detail }) {
    if (detail === 'fade-out') {
      return this.loading = false;
    }
    return this.loading = true;
  }

  _parseResponse({ detail }) {
    this.schedules = detail;
  }

  onPageEnter() {
    this.loading = true;

    const dmSchedule = this.shadowRoot.querySelector('#dmSchedule');
    const abmSchedule = this.shadowRoot.querySelector('#abmSchedule');
    dmSchedule.host = window.AppConfig.host;
    dmSchedule.stationId = this.params.stationId;
    dmSchedule.getSchedules();
    if (abmSchedule) {
      abmSchedule.reset();
    }
  }

  _backPage() {
    this.navigate('stationdetail', { stationId: this.params.stationId });
  }

  _goToLogin() {
    setTimeout(()=> {
      this.navigate('login');
    }, 200);
  }

  _saveSchedule({ detail }) {
    let dmSchedule = this.shadowRoot.querySelector('#dmSchedule');
    detail.stationId = this.params.stationId;
    dmSchedule.saveSchedule(detail);
    this._backPage();
  }

  render() {

    return html`
      <cells-template-paper-drawer-panel disable-edge-swipe="true" header-fixed="true">
        <div slot="app__header">
          <bbva-header-main 
            accessibility-text-icon-left1="Return" 
            icon-left1="coronita:return-12" 
            text="Detalle de Recordatorios" 
            @header-icon-left1-click="${ this._backPage}">
          </bbva-header-main>
        </div>
        <div slot="app__main">
          <dm-schedule 
            id="dmSchedule" 
            @schedule_success="${ this._parseResponse}" 
            @service_loaded="${this._hideSpinner}" 
            @user_unauthorized="${ this._goToLogin}"
          ></dm-schedule>
         
         ${ this.params && this.params.stationId ? html` 
          <abm-schedules id="abmSchedule" @schedule_saved="${ this._saveSchedule}"> </abm-schedules> ` : ''}
            ${ this.loading ? html`<bbva-spinner id="bbvaSpinner" with-mask=""></bbva-spinner>` : ''}
        </div>
      </cells-template-paper-drawer-panel> 
    `;
  }
}

customElements.define('schedule-add-page', ScheduleAddPage);