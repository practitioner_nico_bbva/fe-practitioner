import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';
import '@bbva-web-components/bbva-spinner';
import '@bbva-web-components/bbva-header-main';
import '@practitioner/dm-schedule';
import '@practitioner/list-schedules';


class ScheduleProfilePage extends CellsPage {

  static get properties() {
    return {
      loading: Boolean,
      schedules: Array,
      params: {
        type: Object
      },
    };
  }

  static get styles() {
    return css`
      /* bbva-spinner { height: 100vh; position: fixed; width: 100%; z-index: 1000; background: white; top: 0; } */
    `;
  }

  constructor() {
    super();
    this.loading = true;
  }

  _hideSpinner({ detail }) {
    if (detail === 'fade-out') {
      return this.loading = false;
    }
    return this.loading = true;
  }

  _parseResponse({ detail }) {
    this.schedules = detail;
  }

  onPageEnter() {
    this.loading = true;
    const dmSchedule = this.shadowRoot.querySelector('#dmSchedule');
    dmSchedule.host = window.AppConfig.host;
    dmSchedule.stationId = this.params.stationId;
    dmSchedule.getSchedules();
  }

  _backPage() {
    this.navigate('mapSection');
  }

  _goToLogin() {
    setTimeout(()=> {
      this.navigate('login');
    }, 200);
  }

  _deleteSchedule({ detail }) {
    let dmSchedule = this.shadowRoot.querySelector('#dmSchedule');
    dmSchedule.deleteSchedule(detail.id);
  }

  render() {

    return html`
      <cells-template-paper-drawer-panel disable-edge-swipe="true" header-fixed="true">
        <div slot="app__header">
          <bbva-header-main 
            accessibility-text-icon-left1="Return" 
            icon-left1="coronita:return-12" 
            text="Detalle de Recordatorios" 
            @header-icon-left1-click="${ this._backPage }">
          </bbva-header-main>
        </div>
        <div slot="app__main">
          <dm-schedule 
            id="dmSchedule" 
            @schedule_success="${ this._parseResponse}" 
            @service_loaded="${this._hideSpinner}" 
            @user_unauthorized="${ this._goToLogin }"
          ></dm-schedule>
         
         ${ this.schedules ? html` 
          <list-schedules id="listSchedule" .schedulesList="${ this.schedules }" @delete_schedule="${ this._deleteSchedule }"> </list-schedules> ` : ''}
            ${ this.loading ? html`<bbva-spinner id="bbvaSpinner" with-mask=""></bbva-spinner>` : ''}
        </div>
      </cells-template-paper-drawer-panel> 
    `;
  }
}

customElements.define('schedule-profile-page', ScheduleProfilePage);