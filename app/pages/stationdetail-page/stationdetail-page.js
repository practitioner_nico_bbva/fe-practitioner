import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';
import '@practitioner/station-detail';
import '@practitioner/station-status-dm';
import '@bbva-web-components/bbva-spinner';
import '@bbva-web-components/bbva-header-main';


class StationdetailPage extends CellsPage {

  static get properties() {
    return {
      loading: Boolean,
      params: {
        type: Object
      },
      currentStatus: Object,
      stationInfo: Object
    };
  }

  static get styles() {
    return css`
      bbva-spinner { height: 100vh; position: fixed; width: 100%; z-index: 1000; background: white; top: 0; }
      cells-map { height: 30vh; }
    `;
  }

  constructor() {
    super();
    this.loading = true;
  }

  _hideSpinner({ detail }) {
    if (detail === 'fade-out') {
      this.loading = false;
    }
  }

  _parseResponse({ detail }) {
    const { currentStatus, stationInfo } = detail;
    this.currentStatus = currentStatus;
    this.stationInfo = stationInfo;
  }

  onPageEnter() {
    this.loading = true;
    let stationStatusDm = this.shadowRoot.querySelector('#stationStatusDm');
    stationStatusDm.host = window.AppConfig.host;
    stationStatusDm.stationId = this.params.stationId;
    stationStatusDm.host = window.AppConfig.host;
    stationStatusDm.callService();
  }

  _backPage() {
    this.navigate('mapSection');
  }

  _schedulePage() {
    this.navigate('schedule-add', { stationId: this.params.stationId });
  }

  render() {

    return html`
      <cells-template-paper-drawer-panel disable-edge-swipe="true" header-fixed="true">
        <div slot="app__header">
          <bbva-header-main accessibility-text-icon-left1="Return" icon-left1="coronita:return-12" ccessibility-text-icon-right1="Programar" icon-right1="coronita:frequency" text="Detalle de Estación" @header-icon-left1-click="${ this._backPage}" @header-icon-right1-click="${ this._schedulePage}" ></bbva-header-main>
        </div>
        <div slot="app__main">
          ${ this.stationInfo ? html`
          <cells-map
            api-key="AIzaSyAAoZgZSu47skks2tq0U_mQoEgeOCvSLgs" 
            .me="${ this.stationInfo.geolocation}"
            .location="${ this.stationInfo.geolocation}"
            .hideMe="${ false }"
            .locationIcon="${ 'http://www.myiconfinder.com/uploads/iconsets/32-32-89fdce5084dbe77556cf99f7b22ae7e8-pin.png' }"
            .locationEnabled="${false}"
            .disabledDraggable="${true}"
          ></cells-map> ` : ''}

          <station-status-dm id="stationStatusDm" @list_stations_success_data="${ this._parseResponse}" @service_loaded="${this._hideSpinner}"></station-status-dm>  
         
         ${ this.currentStatus && this.stationInfo ? html` 
            <station-detail id="stationDetail" .currentStatus="${ this.currentStatus}" .stationInfo="${this.stationInfo}" ></station-detail> ` : ''}
            ${ this.loading ? html`<bbva-spinner id="bbvaSpinner" with-mask=""></bbva-spinner>` : ''}
        </div>
      </cells-template-paper-drawer-panel> 
    `;
  }
}

customElements.define('stationdetail-page', StationdetailPage);