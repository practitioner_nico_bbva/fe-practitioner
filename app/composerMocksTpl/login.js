'use strict';

module.exports = (CONFIG) => {
  return {
    template: {
      tag: "cells-template-paper-drawer-panel",
      properties: {
        disableEdgeSwipe: true,
        headerFixed: true
      }
    },
    "components": [
      {
        zone: "app__header",
        familyPath: "@bbva-web-components/bbva-header-main",
        tag: "bbva-header-main",
        render: "litElement",
        properties: {
          ccessibilityTextTconRight1: "Programar",
          iconLeft1: "coronita:return-12",
          text: "Iniciar sesión",
          cellsConnections: {
            out: {
              ch_back: {
                bind: "header-icon-left1-click",
                link: {
                  page: "mapSection"
                }
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        familyPath: "@practitioner/auth-dm",
        tag: "auth-dm",
        render: "litElement",
        properties: {
          host: CONFIG.host,
          cellsConnections: {
            in: {
              ch_sign_up: {
                bind: "createUser"
              },
              ch_sign_in: {
                bind: "loginUser"
              },
              ch_back: {
                bind: "backPage"
              }
            },
            out: {
              ch_show_message: {
                bind: "show_message"
              },
              ch_message_info: {
                bind: "message_response"
              },
              ch_service_loaded: {
                bind: 'service_loaded'
              },

            }
          }
        }
      },
      {
        zone: "app__main",
        familyPath: "@bbva-web-components/bbva-spinner",
        tag: "bbva-spinner",
        render: "litElement",
        fullHeight: true,
        properties: {
          className: "display-none",
          withMask: true,
          cellsConnections: {
            in: {
              ch_service_loaded: {
                bind: "className"
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        familyPath: "@practitioner/sign-up-in",
        tag: "sign-up-in",
        render: "litElement",
        properties: {
          cellsConnections: {
            out: {
              ch_sign_up: {
                bind: "sign_up_user"
              },
              ch_sign_in: {
                bind: "sign_in_user"
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        tag: "cells-alert-box",
        properties: {
          acceptButton: "Aceptar",
          title: "cells-alert-box-title",
          cellsConnections: {
            in: {
              ch_show_message: {
                bind: "open"
              },
              ch_message_info: {
                bind: 'subtitle'
              },
              ch_close_alert: {
                bind: "close"
              }
            },
            out: {
              ch_close_alert: {
                bind: "accept"
              }
            }
          }
        }
      }
    ]
  }
}
