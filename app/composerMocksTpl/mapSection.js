'use strict';

module.exports = (CONFIG) => {
  return {
    template: {
      tag: "cells-template-paper-drawer-panel",
      properties: {
        disableEdgeSwipe: true,
        headerFixed: true
      }
    },
    "components": [
      {
        zone: "app__header",
        familyPath: "@bbva-web-components/bbva-header-main",
        tag: "bbva-header-main",
        render: "litElement",
        properties: {
          ccessibilityTextTconRight1: "Programar",
          iconRight1: "coronita:frequency",
          iconLeft1: "coronita:on",
          text: "Estaciones de Ecobici",
          cellsConnections: {
            in: {
              ch_session_state: {
                bind: "iconLeft1"
              }
            },
            out: {
              navigate_to_schedules: {
                bind: "header-icon-right1-click",
                link: {
                  page: "schedule-profile",
                }
              },
              ch_open_alert: {
                bind: "header-icon-left1-click"
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        familyPath: "@practitioner/bicycle-dm",
        tag: "bicycle-dm",
        render: "litElement",
        properties: {
          host: CONFIG.host,
          cellsConnections: {
            in: {
              '__bridge_evt_page-ready': {
                bind: "loadList"
              },
              ch_station_selected: {
                bind: "changePage"
              }
            },
            out: {
              list_stations_success_data: {
                bind: "list_stations_success_data"
              },
              service_loaded: {
                bind: "service_loaded"
              },
              ch_open_alert_error: {
                bind: "list_stations_error_data"
              },
              ch_load_detail_page: {
                bind: "load_detail_station",
                link: {
                  page: "stationdetail",
                  params: {
                    stationId: "stationId"
                  }
                }
              }
            }
          }
        }
      }, 
      {
        zone: "app__main",
        familyPath: "@practitioner/auth-dm",
        tag: "auth-dm",
        render: "litElement",
        properties: {
          host: CONFIG.host,
          cellsConnections: {
            in: {
              "__bridge_evt_page-ready": {
                bind: "validateSession"
              },
              ch_log_out: {
                bind: "logOut"
              }
            },
            out: {
              ch_session_state: {
                bind: "session_state"
              },
              ch_close_alert: {
                bind: "close_alert"
              }
            }
          }
        }
      },      
      {
        zone: "app__main",
        familyPath: "@bbva-web-components/bbva-spinner",
        tag: "bbva-spinner",
        render: "litElement",
        properties: {
          withMask: true,
          cellsConnections: {
            in: {
              service_loaded: {
                bind: "className"
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        tag: "cells-map",
        properties: {
          locationIcon: "http://www.myiconfinder.com/uploads/iconsets/32-32-89fdce5084dbe77556cf99f7b22ae7e8-pin.png",
          apiKey: "AIzaSyAAoZgZSu47skks2tq0U_mQoEgeOCvSLgs",
          me: { latitude: -34.597963, longitude: -58.3700758 },
          enableDisplayPois: true,
          iconCenter: 'coronita:mylocation',
          cellsConnections: {
            in: {
              "__bridge_evt_parse-route": {
                bind: "askForLocation"
              },
              list_stations_success_data: {
                bind: "markers"
              }
            },
            out: {
              ch_station_selected: {
                bind: "cells-map-marker-clicked",
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        tag: "cells-alert-box",
        properties: {
          subtitle: "No se encunetra disponible en este momento",
          acceptButton: "Aceptar",
          cellsConnections: {
            in: {
              ch_open_alert_error: {
                bind: "open"
              },
              ch_log_out_subtitle: {
                bind: "subtitle"
              }
            }
          }
        }
      },
      {
        zone: "app__main",
        tag: "cells-alert-box",
        properties: {
          subtitle: "Si continúa cerrará la sesión.",
          acceptButton: "Aceptar",
          cellsConnections: {
            in: {
              ch_open_alert: {
                bind: "open"
              },
              ch_close_alert: {
                bind: "opened"
              }
            },
            out: {
              ch_log_out: {
                bind: "accept"
              }
            }
          }
        }
      }
    ]
  }
}