(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'mapSection': '/',
      'login': '/login',
      'stationdetail': '/station/detail/:stationId',
      'schedule-add': '/user/schedule/:stationId',
      'schedule-profile': '/user/schedule/'
    }
  });
}(document));
