// Import here your LitElement initial components (critical / startup)
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '../elements/theme';
// Auto generated imports below. DO NOT remove!
// will be replaced with imports
// ${filledByCellsWithAutoImports}
import '../pages\/schedule-add-page/schedule-add-page.js';
import '../pages\/schedule-profile-page/schedule-profile-page.js';
import '../pages\/stationdetail-page/stationdetail-page.js';
import '@bbva-web-components/bbva-header-main/bbva-header-main.js';
import '@practitioner/auth-dm/auth-dm.js';
import '@bbva-web-components/bbva-spinner/bbva-spinner.js';
import '@practitioner/sign-up-in/sign-up-in.js';
import '@practitioner/bicycle-dm/bicycle-dm.js';