# ECOBICI APP

Esta aplicación fue diseñada para informar a sus usuarios el status de una estación de bicicletas determinada.  

## USO

### 1
Es necesario habilitar la geolocalización.

![Geolocalización](documentation/location.png "Geolocalización")  

____

### 2

- En la parte de la cabecera se encuentran los recordatorios.
- Sobre el mapa, se encuentran marcadas las estaciones.
- A la derecha en la parte inferior, sobre el mapa se encuentra el botón para geolocalizar.

![Principal](documentation/main.png "Principal")  

____

### 3

Al presionar sobre una de las estaciones, navegamos hasta la pantalla de detalle de estaciones, donde podemos visualizar:

* Nombre
* Dirección
* Bicicletas disponibles
* Última fecha de actualización
* Disponibilidad de Bicicletas
* Disponibilidad de Slots
* Y cuáles son los medios para retirar una bici.  

En la cabecera, encontramos en la parte superior, la flecha para regresar al mapa y en la parte izquierda el botón para **agendar un nuevo recordatorio para esa estación.**
  

![detail](documentation/detail.png)

### 4

Si el usuario no se encuentra logueado, se presentará la pantalla de login

![login](documentation/login.png)

### 5

El usuario puede registrarse.

![signin](documentation/signIn.png)

### 6

Una vez que el usuario inició sesión, puede visualizar la página para agregar un nuevo recordatorio.  

![alta](documentation/altaRecordatorio.png)

### 7 
Regresando al mapa principal, encontramos en la cabecera en la parte derecha, un botón que permite acceder a la lista de recordatorios.  
Si usuario inició sesión, verá la siguiente pantalla. Aquí eliminar un recordatorio si así lo desea.

![alta](documentation/scheduleList.png)


### 8  
Regresando al mapa principal, encontramos en la cabecera en la parte izquierda, un botón para cerrar sesión.

![logout](documentation/logOut.png)

## Componentes  
_todos los componentes contienen código comentado_

**Visuales**  
* [abm-schedules](https://bitbucket.org/practitioner_nico_bbva/abm-schedule/src/master/)  
* [list-schedules](https://bitbucket.org/practitioner_nico_bbva/list-schedules/src/master/)  
* [sign-up-in](https://bitbucket.org/practitioner_nico_bbva/sign-up-in)  
* [station-detail](https://bitbucket.org/practitioner_nico_bbva/station-detail)  

**Data Managers**  
* [auth-dm](https://bitbucket.org/practitioner_nico_bbva/auth-dm)  
* [bicycle-dm](https://bitbucket.org/practitioner_nico_bbva/bicycle-dm)  
* [dm-schedule](https://bitbucket.org/practitioner_nico_bbva/dm-schedule)  
* [station-status-dm](https://bitbucket.org/practitioner_nico_bbva/station-status-dm)  

**Utils**  
[security-module](https://bitbucket.org/practitioner_nico_bbva/security-module)  

## Tecnologías

* Se creó bajo la estrucutra de **CELLS 7.7.4** y cuenta con 2 páginas dinámicas y otras 3 páginas estáticas.
* Se utiliza **Polymer 2** para algunos componentes y **Lit Elelements** para todos los componentes propios.

## Instalación con Docker  

1. descargar la imagen de ecobici-app  
> $ docker pull nicoc123/ecobici-app

2. Ejecutar la API en la red ecobici  
> $ sudo docker run -p 1123:1123 nicoc123/ecobici-app

## Acceso
> http://localhost:1123